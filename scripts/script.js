// Typekit (Custom Fonts)
(function(d) {
    var config = {
            kitId: 'ixf7pyq',
            scriptTimeout: 3000,
            async: true
        },
        h = d.documentElement,
        t = setTimeout(function() {
            h.className = h.className.replace(/\bwf-loading\b/g, "") + " wf-inactive";
        }, config.scriptTimeout),
        tk = d.createElement("script"),
        f = false,
        s = d.getElementsByTagName("script")[0],
        a;
    h.className += " wf-loading";
    tk.src = 'https://use.typekit.net/' + config.kitId + '.js';
    tk.async = true;
    tk.onload = tk.onreadystatechange = function() {
        a = this.readyState;
        if (f || a && a != "complete" && a != "loaded") return;
        f = true;
        clearTimeout(t);
        try {
            Typekit.load(config)
        } catch (e) {}
    };
    s.parentNode.insertBefore(tk, s)
})(document);

// Google Analytics
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-72159708-1', 'auto');
ga('send', 'pageview');

// Main
jQuery(function($) {

    var zack = {

        init: function() {
            // Fire Them Up
            zack.fastclick(); // fastclick
            zack.flexslider(); // flexslider
            zack.pagepiling(); // pagpiling
            // zack.loop(); // loop video
            zack.mobile(); // mobile nav
            // zack.button(); // button
        },

        fastclick: function() {
            $(function() {
                FastClick.attach(document.body);
            });
        },

        flexslider: function() {
            $('.slider').flexslider({
                animation: "slide",
                slideshow: "true",
                slideshowSpeed: "6000",
                animationSpeed: "500",
                pauseOnHover: true,
                touch: true,
                directionNav: true,
                controlNav: false,
                animationLoop: true,
                prevText: "",
                nextText: "",
                keyboard: true,
                slideshow: false,
                multipleKeyboard: true
            });
        },

        pagepiling: function() {

            $('#pagepiling').pagepiling({
                menu: '#menu',
                anchors: ['home', 'about', 'experience', 'races', 'syracuse', 'fundraising', 'photo'],
                sectionsColor: ['#000000', '#000000', '#000000', '#000000'],
                navigation: {
                    'position': 'right',
                    'bulletsColor': '#fff'
                },
                loopBottom: true,
                css3: true,
                easing: 'swing',
                normalScrollElements: '#section2',
                keyboardScrolling: false,
                // afterRender: function(){
                    // play the video
                    // $('video').get(0).play();
                // },
                // afterLoad: function(anchorLink, index){
                //     // Play On First Section
                //     if (index == 1) {
                //         $('video').get(0).play();
                //     } else {
                //         $('video').get(0).pause();
                //     }
                // }
            });

        },

        // loop: function() {
        //     $(document).on('click touch', 'video', function() {
        //         // this[this.paused ? 'play' : 'pause']();
        //         $('video').prop('muted') ? $("video").prop('muted', false) : $("video").prop('muted', true);
        //     });
        //     $('video').on({
        //         mouseenter: function () {
        //             $('.notification').fadeIn(333);
        //         },
        //         mouseleave: function () {
        //             $('.notification').fadeOut(333);
        //         }
        //     });
        // },

        mobile: function() {
            $('.mobile-menu').on('click', function(e) {
                e.preventDefault();
                if ($(window).width() < 899) {
                    $('li.nav, li.social').slideToggle(333);
                }
            });
            if ($(window).width() < 899) {
                $('#menu li.nav a').on('click', function(e) {
                    e.preventDefault();
                    $('li.nav, li.social').slideToggle(333);
                });
            }
        },

        resize: function() {
            if ($(window).width() > 899) {
                $('li.nav, li.social').show();
            } else {
                $('li.nav, li.social').hide();
            }
        },

        // button: function() {
        //     $(document).on('click touch', '#section7 button', function() {
        //         window.location.href = "http://zlevphotography.com/";
        //     });
        // }

    }

    zack.init();

    // Window Resize
    $(window).on('resize', function() {
        zack.resize();
    });

});
