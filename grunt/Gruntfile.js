///////////////////////////////////////
// Grunt Config
///////////////////////////////////////

module.exports = function(grunt) {
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        grunt: {
            files: ["Gruntfile.js"],
            tasks: ["default"]
        },
        watch: {
            options: {
                dateFormat: function(time) {
                    grunt.log.writeln('Watch finished in ' + time + 'ms at' + (new Date()).toString());
                    grunt.log.writeln('Waiting for more changes...');
                    grunt.log.writeln(' ');
                    grunt.log.writeln(' ');
                },
            },
            css: {
                files: ['../sass/*.scss'],
                tasks: ['sass', 'cssmin'],
            },
            scripts: {
                files: '../scripts/*.js',
                tasks: ['uglify'],
                options: {
                    interrupt: true,
                },
            },
        },
        sass: {
            dist: {
                files: {
                    '../css/style.css': '../sass/style.scss'
                }
            }
        },
        cssmin: {
            css: {
                src: ['../css/reset.css', '../css/font-awesome.css', '../css/foundation.css', '../css/flexslider.css', '../css/jquery.pagepiling.css', '../css/style.css'],
                dest: '../style.min.css'
            }
        },
        uglify: {
            options: {
                mangle: true
            },
            my_target: {
                files: {
                    '../script.min.js': [
                        '../scripts/jquery.js',
                        // '../scripts/handlebars.js',
                        '../scripts/fastclick.js',
                        '../scripts/flexslider.js',
                        '../scripts/jquery.pagepiling.js',
                        '../scripts/script.js'
                    ]
                }
            }
        },
        notify_hooks: {
            options: {
                enabled: true,
                max_jshint_notifications: 5,
                title: "Grunt: Zack Levandov",
                success: true,
                duration: 3
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-notify');

    grunt.registerTask('default', ['sass', 'cssmin', 'uglify', 'watch']);

    // Run Tasks
    grunt.task.run('notify_hooks');

}
